package br.edu.undb.cafufa.controller;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.edu.undb.cafufa.model.Evento;
import br.edu.undb.cafufa.model.Loja;
import br.edu.undb.cafufa.repository.Eventos;
import br.edu.undb.cafufa.repository.Lojas;

public class DemonstrativoFinanceiroBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Lojas lojas;

	@Inject
	private Eventos eventos;
	
	public Loja lojaSelecionada;
	public Evento eventoSelecionado;

	public List<Loja> pesquisarLojas(String nome) {
		return this.lojas.queContem(nome);
	}
	
	public List<Evento> pesquisarEventos(String nome) {
		return this.eventos.queContem(nome);
	}

	public Loja getLojaSelecionada() {
		return lojaSelecionada;
	}

	public void setLojaSelecionada(Loja lojaSelecionada) {
		this.lojaSelecionada = lojaSelecionada;
	}

	public Evento getEventoSelecionado() {
		return eventoSelecionado;
	}

	public void setEventoSelecionado(Evento eventoSelecionado) {
		this.eventoSelecionado = eventoSelecionado;
	}

}
