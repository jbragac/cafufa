package br.edu.undb.cafufa.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.edu.undb.cafufa.model.Loja;

public class Lojas implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager manager;

	@Inject
	public Lojas(EntityManager manager) {
		this.manager = manager;
	}

	public Loja porId(Long id) {
		return manager.find(Loja.class, id);
	}

	public List<Loja> queContem(String nome) {
		TypedQuery<Loja> query = manager.createQuery(
		"from Loja where nome like :nome",Loja.class);
		query.setParameter("nome", "%" + nome + "%");
		return query.getResultList();
	}

}