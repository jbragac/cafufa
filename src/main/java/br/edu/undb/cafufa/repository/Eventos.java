package br.edu.undb.cafufa.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.edu.undb.cafufa.model.Evento;

public class Eventos implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager manager;

	@Inject
	public Eventos(EntityManager manager) {
		this.manager = manager;
	}

	public Evento porId(Long id) {
		return manager.find(Evento.class, id);
	}
	
	public List<Evento> queContem(String nome) {
		TypedQuery<Evento> query = manager.createQuery(
		"from Evento where nome like :nome",Evento.class);
		query.setParameter("nome", "%" + nome + "%");
		return query.getResultList();
	}
	
}