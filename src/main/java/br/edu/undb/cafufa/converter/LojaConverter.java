package br.edu.undb.cafufa.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.edu.undb.cafufa.model.Loja;
import br.edu.undb.cafufa.repository.Lojas;

@FacesConverter(forClass = Loja.class)
public class LojaConverter implements Converter {

	@Inject // funciona graças ao OmniFaces
	private Lojas lojas;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Loja retorno = null;
		if (value != null) {
			retorno = this.lojas.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Loja) value).getId().toString();
		}
		return null;
	}

}